import logging
from http.server import HTTPServer, BaseHTTPRequestHandler

class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/plain')
        self.end_headers()
        self.wfile.write(b'This is a sample for CI/CD pipeline')

def run(server_class=HTTPServer, handler_class=SimpleHTTPRequestHandler, port=8000):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    logging.info(f'Starting server on port http://localhost:{port}...')
    
    # Serve one request and then exit
    httpd.handle_request()

if __name__ == '__main__':
    # Configure logging
    logging.basicConfig(level=logging.INFO)

    # Start the server
    run()
